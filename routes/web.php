<?php
/*CourseAssignToTeacher*/
Route::get('/admin/CourseAssignToTeacher/create','CourseAssignToTeacher@create');

//Teacher route
Route::get('/admin/teacher/create','TeacherController@create');
Route::post('/admin/teacher/store','TeacherController@store');
Route::get('/admin/teacher/index','TeacherController@index');
Route::get('/admin/teacher/{id}/edit','TeacherController@edit');
Route::post('/admin/teacher/{id}/update','TeacherController@update');
Route::post('/admin/teacher/{id}/delete','TeacherController@destroy');
//course route
Route::get('admin/course/create','CourseController@create');
Route::post('/admin/course/store','CourseController@store');
Route::get('/admin/course/index','CourseController@index');
Route::get('/admin/course/{id}/edit','CourseController@edit');
Route::post('/admin/course/{id}/update','CourseController@update');
Route::post('/admin/course/{id}/delete','CourseController@destroy');


//user route
Route::resource('users','UserController');
Route::post('users/{id}/edit','UserController@edit');
//Department route
Route::resource('departments','DepartmentController');
Route::post('departments/{id}/edit','DepartmentController@edit');

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin', function () {
    return view('admin.layout.master');
});
Route::get('/admin/user', function () {
    return view('admin.user.index');
});
Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

