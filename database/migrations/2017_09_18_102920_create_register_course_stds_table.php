<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterCourseStdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_course_stds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Student_id');
            $table->string('name');
            $table->string('eamil');
            $table->string('department');
            $table->string('slect_course');
            $table->date('date_entry');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_course_stds');
    }
}
