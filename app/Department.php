<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table ='departmetns';


    public function course()
    {
        return $this->hasMany('App\Course','department_id','id');
    }
    public function teacher()
    {
        return $this->hasMany('App\Course','department_id','id');
    }
    public function student()
    {
        return $this->hasMany('App\StudentRegister','department_id','id');
    }
    public function roomAllocate()
    {
        return $this->hasMany('App\AllocateClassRoom','department_id','id');
    }
}
