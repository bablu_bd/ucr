<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function department()
    {
        return $this->hasTo('App\Department','department_id');
    }
    public function course ()
    {
        return $this->manyTo('App\CourseAssign','course_id');
    }
}
