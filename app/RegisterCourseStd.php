<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterCourseStd extends Model
{
    public function courseRegister()
    {
        return $this->hasTO('App\StudentRegister','student_id');
    }
}
