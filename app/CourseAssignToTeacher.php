<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAssign extends Model
{
    public function  teacher()
    {
        return $this->hasMany('App\Teacher','teacher_id');
    }
    public function department()
    {
        return $this->hasMany('App\Department','department_id');
    }
}
