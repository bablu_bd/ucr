<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Department;
use Session;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = Teacher::all();
        return view('admin.teacher.index',['allUser'
        => $allUser]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::all();
        return view('admin.teacher.create')
            ->with('department',$department);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'email'  =>'required',
            'contact'=>'required|regex:/(01)[0-9]{9}/',
            'designation'=>'required',
            'credit'=>'required',

        ]);
        $obj = new Teacher();
        $obj->name =$request->name;
        $obj->address =$request->address;
        $obj->email =$request->email;
        $obj->contact=$request->contact;
        $obj->designation=$request->designation;
        $obj->department =$request->department;
        $obj->credit =$request->credit;
        $obj->save();
        Session::flash('message','Teacher Add Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::all();
        $data = Teacher::find($id);
        return view('admin.teacher.edit')
            ->with('data',$data)
            ->with('department',$department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
            'contact'=>'required|numeric|size:11',
            'designation'=>'required',
            'credit'=>'credit'
        ]);
        $existingData = Teacher::find($id);
        $existingData->name=$request->name;
        $existingData->address=$request->address;
        $existingData->email=$request->email;
        $existingData->contact=$request->contact;
        $existingData->designation=$request->designation;
        $existingData->department=$request->department;
        $existingData->credit=$request->credit;
        $existingData->update();
        session::flash('message','Teacher Update Successfully');
        return redirect('admin/teacher/index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Teacher::find($id);
        $data->delete();
        Session::flash('message','Teacher Delete Successfully');
        return redirect('admin.teacher.index');
    }
}
