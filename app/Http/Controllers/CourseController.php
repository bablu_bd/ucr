<?php

namespace App\Http\Controllers;

use App\Course;
use App\Department;
use Illuminate\Http\Request;
use Session;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = Course::all();
        return view('admin.course.index',['allUser' => $allUser]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deparment = Department::all();

        return view('admin.course.create')
            ->with('department',$deparment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code'=>'required|min:3|max:5',
            'credit'=>'required|min:0.5|max:5.0',
            'name'=>'required',

        ]);
        $obj = new Course();
        $obj->code =$request->code;
        $obj->name =$request->name;
        $obj->credit=$request->credit;
        $obj->descriptin=$request->descriptin;
        $obj->department_id=$request->department_id;
        $obj->semester=$request->semester;
        $obj->save();
        Session::flash('message','Course Add Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::all();
        $data = Course::find($id);
        return view('admin.course.edit')
        ->with('data',$data)
        ->with('department',$department);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code'=>'required|min:3|max:5',
            'credit'=>'required|min:0.5|max:5.0',
            'name'=>'required',

        ]);

        $existingData = Course::find($id);
        $existingData->code=$request->code;
        $existingData->name=$request->name;
        $existingData->credit=$request->credit;
        $existingData->descriptin=$request->descriptin;
        $existingData->department_id=$request->department;
        $existingData->semester=$request->semester;
        $existingData->save();
        session::flash('message','Course Update successfully');
        return redirect('admin/course/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Course::find($id);
        $data->delete();
        Session::flash('message','Course Successfully Delete');
        return redirect('/admin/course');
    }

}
