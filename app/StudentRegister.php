<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRegister extends Model
{
    public function department()
    {
        return $this->hasTO('App\Department','department_id');
    }
    public function studentregister()
    {
        return $this->manyTO('App\StudentRegister','student_id','id');
    }
    public function saveresult()
    {
        return $this->hasTo('App\SaveStudentResult','department_id','id');
    }

}
