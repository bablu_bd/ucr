<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveStudentResult extends Model
{
    public function studentregister()
    {
        return $this->manyTO('App\StudentRegister','student_id');
    }
    public function department()
    {
        return $this->manyTO('App\Department','department_id');
    }
}
