<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllocateClassRoom extends Model
{
    public function department()
    {
        return $this->hasTo('App\Department','department_id');
    }

}
