@extends('admin.layout.master')

@section('title', 'View All Course')
@section('all_course','Active')
@section('content')
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>

    @endif
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Code</th>
            <th>Name</th>
            <th>Credit</th>
            <th>description</th>
            <th>Department</th>
            <th>Semester</th>
            <th>Action</th>
        </tr>
        @foreach($allUser as $user)
             <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->code}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->credit}}</td>
                <td>{{$user->descriptin}}</td>
                <td>{{$user->department->name}}</td>
                <td>{{$user->semester}}</td>

                <td>
                    <a href="{{url('/admin/course',[$user->id,'edit'])}}">Edit</a>
                    {!! Form::open(['url' => ['admin/course', $user->id,'delete']])!!}
                    {!! Form::submit('Delete')!!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection