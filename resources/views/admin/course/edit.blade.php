@extends('admin.layout.master')

@section('title', 'Add New Course')
@section('add_course','Active')
@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>

    @endif
    {!! Form::model($data,['url' => '/admin/course/'.$data->id.'/update'])!!}

    {!! Form::label('code','Course Code') !!}
    {!! Form::number('code') !!}

    {!! Form::label('name','Course Name') !!}
    {!! Form::text('name') !!}

    {!! Form::label('credit','Course Credit') !!}
    {!! Form::number('credit') !!}

    {!! Form::label('description','Course Description') !!}
    {!! Form::text('descriptin') !!}

    {!! Form::label('department','Department') !!}
    <select name="department">
        @foreach($department as $value)
            <option value="{{ $value->id }}">{{ $value->name }}</option>
        @endforeach
    </select>

    {!! Form::label('semester','Semester') !!}
    {!! Form::select('semester', ['1st semester' => 'First Semester', '2nd Semester' => 'Second Semester','3rd Semester'=>'Third Semester','4th Semester'=>'Four Semester','5th Semester'=>'Five Semester','6th Semester'=>'Six Semester','7th Semester'=>'Seven Semester','8th Semester'=>'Eight Semester'])!!}

    {!! Form::submit('update') !!}
    {!! Form::close() !!}
@endsection