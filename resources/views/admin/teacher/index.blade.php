@extends('admin.layout.master')

@section('title', 'View All Teacher')
@section('view_teacher','Active')
@section('content')
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>

    @endif
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Contact No.</th>
            <th>Designation</th>
            <th>Department</th>
            <th>Credit</th>
            <th>Action</th>
        </tr>
        @foreach($allUser as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->address}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->contact}}</td>
                <td>{{$user->designation}}</td>
                <td>{{$user->department}}</td>
                <td>{{$user->credit}}</td>

                <td>
                    <a href="{{url('/admin/teacher',[$user->id,'edit'])}}">Edit</a>
                    {!! Form::open(['url' => ['admin/teacher', $user->id,'delete']])!!}
                    {!! Form::submit('Delete')!!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection