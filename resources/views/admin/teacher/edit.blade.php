@extends('admin.layout.master')

@section('title', 'View Teacher')
@section('view_teacher','Active')
@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>

    @endif

    {!! Form::model($data,['url' => 'admin/teacher/store']) !!}

    {!! Form::label('name','Name:') !!}
    {!! Form::text('name') !!}

    {!! Form::label('address','Address:') !!}
    {!! Form::textarea('address')!!}

    {!! Form::label('email','Email:') !!}
    {!! Form::email('email') !!}

    {!! Form::label('contact','Contact Number:') !!}
    {!! Form::number('contact') !!}

    {!! Form::label('designation','Designation:') !!}
    {!! Form::select('designation', ['-------'=>'-------------','lecturer' => 'Lecturer', 'senior lecturer' => 'Senior Lecturer','assis. prof'=>'Assistant Professor','asso. prof.'=>'Associate Professor','professor'=>'Professor'])!!}

    {!! Form::label('department','Department:') !!}
    <select name="department">
        @foreach($department as $value)
            <option value="{{ $value->id }}">{{ $value->name }}</option>
        @endforeach
    </select>
    {!! Form::label('credit','Credit:') !!}
    {!! Form::number('credit') !!}

    {!! Form::submit('update') !!}
    {!! Form::close() !!}
@endsection