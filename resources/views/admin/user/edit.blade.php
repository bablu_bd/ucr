@extends('admin.layout.master')
@section('user_title','Edit User')
@section('user_add_menu','active')
@section('content')
        @if(Session::has('message'))
            <button type="button" class="btn btn-success">{{Session::get('message')}}</button>
        @endif

        {!!Form::model($data,['url' =>'/users/'.$data->id,'method'=>'PUT']) !!}
        {!!Form::label('name','Name') !!}
        {!!Form::text('name') !!}

        {!! Form::label('email','Email') !!}
        {!! Form::email('email') !!}

        {!! Form::submit('submit') !!}
        {!! Form::close() !!}
@endsection