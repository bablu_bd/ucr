@extends('admin.layout.master')
@section('view_admin','active')
@section('title')
    {{'View Admin!!'}}
@endsection
@section('content')
    @if(Session::has('message'))
         <button type="button" class="btn btn-success">{{Session::get('message')}}</button>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        @foreach($allUser as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    {!! Form::open(['url' => ['users', $user->id,'edit']]) !!}
                    {!! Form::submit('EDIT', array('class'=>'btn btn-primary'))!!}
                    {!! Form::close() !!}

                    {!!Form::open(['url' => ['users',$user->id],'method'=>'DELETE'])!!}
                    {!! Form::submit('Delete', array('class'=>'btn btn-danger')) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@endsection