@extends('admin.layout.master')
@section('user_title','Add New User')
@section('user_add_menu','active')
@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors-all() as $error)
                    <li>{{$error}}</li>
                 @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <h1><button type="button" class="btn btn-success">{{Session::get('message')}}</button></h1>
    @endif
   {!! Form::open(['url' =>'/users']) !!}
    {!!Form::label('name','Name') !!}
    {!!Form::text('name') !!}

    {!! Form::label('email','Email') !!}
    {!! Form::email('email') !!}
    {!! Form::label('password','Password')!!}
    {!! Form::password('password') !!}
    {!! Form::submit('submit', array('class'=>'btn btn-primary')) !!}
    {!! Form::close() !!}
@endsection