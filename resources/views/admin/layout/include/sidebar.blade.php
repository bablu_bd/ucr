<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li class="active"><a href="{{url('/dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Admin Settings</span></a>
                <ul>
                    <li class="@yield('user_add_menu')"><a href="{{url('/users/create')}}">Add Admin</a></li>
                    <li class="@yield('view_admin')"><a href="{{url('/users')}}">View Admin</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-copy"></i> <span>Department Settings</span></a>
                <ul>
                    <li class="@yield('add_department')"><a href="{{url('/departments/create')}}">Add Department</a></li>
                    <li class="@yield('view_department')"><a href="{{url('/departments')}}">View Department</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-droplet2"></i> <span>Course Settings</span></a>
                <ul>
                    <li class="@yield('add_course')"><a href="{{url('/admin/course/create')}}">Add Course</a></li>
                    <li class="@yield('view_course')"><a href="{{url('/admin/course/index')}}">View Course</a></li>

                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Teacher Settings</span></a>
                <ul>
                    <li class="@yield('add_teacher')"><a href="/admin/teacher/create">Add Teacher</a></li>
                    <li class="@yield('view_teacher')"><a href="/admin/teacher/index">View Teacher</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-users"></i> <span>Course Assign To Teacher Settings</span></a>
                <ul>
                    <li class="@yield('add_assign')"><a href="/admin/CourseAssignToTeacher/create">Add Course Assign</a></li>
                    <li class="@yield('view_assign')"><a href="/admin/CourseAssignToTeacher/index">View Course Assign</a></li>
                </ul>
            </li>


        </ul>
    </div>
</div>