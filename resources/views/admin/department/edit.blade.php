@extends('admin.layout.master')
@section('user_title','Edit Department')
@section('add_department','active')
@section('content')
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>
    @endif

    {!!Form::model($data,['url' =>'/departments'.$data->id,'edit']) !!}
    {!!Form::label('name','Department Name') !!}
    {!!Form::text('name') !!}

    {!! Form::label('code','Department code') !!}
    {!! Form::number('code') !!}

    {!! Form::submit('submit') !!}
    {!! Form::close() !!}
@endsection