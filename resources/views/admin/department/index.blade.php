@extends('admin.layout.master')
@section('view_department','active')
@section('title')
    {{'View Department!!'}}
@endsection
@section('content')
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Code</th>
            <th>Action</th>
        </tr>
        @foreach($allUser as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->code}}</td>
                <td>
                    {!! Form::open(['url' => ['departments', $user->id,'edit']]) !!}
                    {!! Form::submit('EDIT', array('class'=>'btn btn-primary'))!!}
                    {!! Form::close() !!}

                    {!!Form::open(['url' => ['departments',$user->id],'method'=>'DELETE'])!!}
                    {!! Form::submit('Delete', array('class'=>'btn btn-danger')) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@endsection