@extends('admin.layout.master')
@section('main_title','Add Department')
@section('add_department','active')
@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <button type="button" class="btn btn-success">{{Session::get('message')}}</button>

    @endif
    {!! Form::open(['url' => '/departments']) !!}

    {!! Form::label('name','Department Name') !!}
    {!! Form::text(' name') !!}
    {!! Form::label('code','Department code') !!}
    {!! Form::number('code') !!}


    {!! Form::submit('submit', array('class'=>'btn btn-primary')) !!}
    {!! Form::close() !!}
@endsection